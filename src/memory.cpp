#include <memory.hpp>

#include <main.hpp>

#include <sys/stat.h>

#include <fstream>
#include <cstring>

Memory::Memory()
{
	init();
}

void Memory::init()
{
	mapper = Mapper();
	
	mem = new u8[0x10000];
	ppumem = new u8[0x4000];
	
	memset(mem, 0x00, 0x10000);
}

void Memory::loadROM(char* path)
{
	ifstream romfile(path, ios::in | ios::binary);
	
	struct stat sb{};
	stat(path, &sb);
	int romLen = sb.st_size;
	
	char* rom = new char[romLen];
	
	romfile.read(rom, romLen);
	
	char* header = new char[16];
	memcpy(header, rom, 16);
	
	rom = &rom[16];
	
	mapper.mapFromHeader(header, rom);
}

u8 Memory::read(u16 addr)
{
	u8 value;
	
	switch (addr)
	{
		case PPUCTRL:
		case PPUMASK:
		{
			return ppu.latch;
			
			break;
		}
		
		case PPUSTATUS:
		{
			value = mem[PPUSTATUS];
			
			mem[PPUSTATUS] &= 0x7F;
			
			ppu.latchPos = true;
			ppu.latch = 0;
			
			break;
		}
		
		case OAMDATA:
		{
			
			
			break;
		}
		
		// TODO: internal buffer
		case PPUDATA:
		{
			u8 ppuVal = ppu.mem[ppu.inPPUAddr];
			
			ppu.inPPUAddr += ((mem[PPUCTRL] >> 2) & 1) ? 32 : 1;
			
			return ppuVal;
			
			break;
		}
		
		default:
		{
			value = mem[addr];
			
			break;
		}
	}
	
	return value;
}

void Memory::write(u16 addr, u8 value)
{
	switch (addr)
	{
		// TODO: temporary ppuscroll/ppuaddr value should be used
		//       first and *then* copied to the real ppuscroll/ppuaddr
		//       
		//       actually write to ppu latch variable
		case PPUSCROLL:
		{
			ppu.inPPUScroll &= 0xFF << (ppu.latchPos * 8);
			ppu.inPPUScroll |= value << (ppu.latchPos * 8);
			ppu.latchPos ^= true;
			
			break;
		}
		
		case PPUADDR:
		{
			cout << "whoa dude" << endl;
			
			ppu.inPPUAddr &= 0xFF << (ppu.latchPos * 8);
			ppu.inPPUAddr |= value << (ppu.latchPos * 8);
			ppu.latchPos ^= true;
			
			break;
		}
		
		case PPUDATA:
		{
			ppu.mem[ppu.inPPUAddr] = value;
			
			ppu.inPPUAddr += ((mem[PPUCTRL] >> 2) & 1) ? 32 : 1;
			
			break;
		}
	}
	
	mem[addr] = value;
}