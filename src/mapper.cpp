#include <mapper.hpp>

#include <main.hpp>

#include <cstring>

void Mapper::mapFromHeader(char* header, char* rom)
{
	// TODO: handle trainer
	
	if (((header[7] >> 3) & 1) == 1 && ((header[7] >> 2) & 1) == 0)
	{
		printf("E: NES 2.0 ROM given. Exiting...");
		quitNES();
	}
	
	u8 prgNo = header[4];
	u8 chrNo = header[5];
	
	int prgSize = 0x4000 * prgNo;
	
	if (prgNo == 1)
	{
		memcpy(&cpu.mmu.mem[0xC000], rom, 0x4000);
	}
	
	else
	{
		// first PRG bank
		memcpy(&cpu.mmu.mem[0x8000], rom, 0x4000);
		
		// last PRG bank
		memcpy(&cpu.mmu.mem[0xC000], &rom[prgSize - 0x4000], 0x4000);
	}
	
	// CHR bank
	memcpy(ppu.mem, &rom[prgSize], 0x2000);
}