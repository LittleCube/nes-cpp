#include <common.hpp>

#include <main.hpp>
#include <ppu.hpp>

#include <drivers.hpp>

#include <SDL2/SDL.h>

CPU cpu;
PPU ppu;

bool quit;

int main(int argc, char** argv)
{
	cpu = CPU();
	ppu = PPU();
	
	if (argc > 1)
	{
		cpu.loadROM(argv[1]);
		
		printf("N: ROM loaded! reset vector: 0x%02X%02X\n", cpu.mmu.mem[0xFFFD], cpu.mmu.mem[0xFFFC]);
	}
	
	else
	{
		printf("\nE: no ROM given\n");
		printf("N: `nes [rompath]`\n\n");
		return 1;
	}
	
	quit = false;
	
	initDrivers();
	
	while (!quit)
	{
		cpu.cycle();
		
		ppu.clock(cpu.m);
	}
}

void quitNES()
{
	exit(0);
}