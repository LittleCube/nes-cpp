#include <cpu.hpp>

CPU::CPU()
{
	init();
}

inline void CPU::init()
{
	m = 0;
	totalCycles = 0;
	
	a = 0;
	x = 0;
	y = 0;
	
	p = 0x24;
	
	sp = 0xFD;
	
	pc = 0;
}



void CPU::loadROM(char* path)
{
	mmu.loadROM(path);
	
	pc = (mmu.read(0xFFFD) << 8) | mmu.read(0xFFFC);
}



void CPU::cycle()
{
	m = 0;
	
	u8 opcode = mmu.mem[pc];
	
	switch (opcode)
	{
		case 0x69:	// ADC
		{
			adc(mmu.read(pc + 1));
			
			pc += 2;
			m += 2;
			break;
		}
		
		case 0x65:
		{
			adc(mmu.read(mmu.read(pc + 1)));
			
			pc += 2;
			m += 3;
			break;
		}
		
		case 0x75:
		{
			adc(zeroPageX(mmu.read(pc + 1)));
			
			pc += 2;
			m += 4;
			break;
		}
		
		case 0x6D:
		{
			adc(absolute(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0x7D:
		{
			adc(absoluteX(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0x79:
		{
			adc(absoluteY(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0x61:
		{
			adc(indirectX(mmu.read(pc + 1)));
			
			pc += 2;
			m += 6;
			break;
		}
		
		case 0x71:
		{
			adc(indirectY(mmu.read(pc + 1)));
			
			pc += 2;
			m += 5;
			break;
		}
		
		case 0x29:	// AND
		{
			_and(mmu.read(pc + 1));
			
			pc += 2;
			m += 2;
			break;
		}
		
		case 0x25:
		{
			_and(mmu.read(mmu.read(pc + 1)));
			
			pc += 2;
			m += 3;
			break;
		}
		
		case 0x35:
		{
			_and(zeroPageX(mmu.read(pc + 1)));
			
			pc += 2;
			m += 4;
			break;
		}
		
		case 0x2D:
		{
			_and(absolute(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0x3D:
		{
			_and(absoluteX(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0x39:
		{
			_and(absoluteY(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0x21:
		{
			_and(indirectX(mmu.read(pc + 1)));
			
			pc += 2;
			m += 6;
			break;
		}
		
		case 0x31:
		{
			_and(indirectY(mmu.read(pc + 1)));
			
			pc += 2;
			m += 5;
			break;
		}
		
		case 0x0A:	// ASL
		{
			asl(a);
			
			pc += 1;
			m += 2;
			break;
		}
		
		case 0x06:
		{
			asl(mmu.read(mmu.read(pc + 1)));
			
			pc += 2;
			m += 5;
			break;
		}
		
		case 0x16:
		{
			asl(zeroPageX(mmu.read(pc + 1)));
			
			pc += 2;
			m += 6;
			break;
		}
		
		case 0x0E:
		{
			asl(absolute(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 6;
			break;
		}
		
		case 0x1E:
		{
			// use different absolute X method with no page-cross check
			asl(absoluteX_noClocks(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 7;
			break;
		}
		
		case 0x90:	// BCC
		{
			rbr(!c, mmu.read(pc + 1));
			
			m += 2;
			break;
		}
		
		case 0xB0:	// BCS
		{
			rbr(c, mmu.read(pc + 1));
			
			m += 2;
			break;
		}
		
		case 0xF0:	// BEQ
		{
			rbr(z, mmu.read(pc + 1));
			
			m += 2;
			break;
		}
		
		case 0x24:	// BIT
		{
			bit(mmu.read(mmu.read(pc + 1)));
			
			pc += 2;
			m += 3;
			break;
		}
		
		case 0x2C:
		{
			bit(absolute(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0x30:	// BMI
		{
			rbr(n, mmu.read(pc + 1));
			
			m += 2;
			break;
		}
		
		case 0xD0:	// BNE
		{
			rbr(!z, mmu.read(pc + 1));
			
			m += 2;
			break;
		}
		
		case 0x10:	// BPL
		{
			rbr(!n, mmu.read(pc + 1));
			
			m += 2;
			break;
		}
		
		case 0x00:	// BRK
		{
			push(pc >> 8);
			push((pc & 0xFF) + 2);
			push(p | 0x30);
			
			pc = addr16(mmu.read(0xFFFE), mmu.read(0xFFFF));
			
			b = 1;
			i = 1;
			
			m += 7;
			break;
		}
		
		case 0x50:	// BVC
		{
			rbr(!v, mmu.read(pc + 1));
			
			m += 2;
			break;
		}
		
		case 0x70:	// BVS
		{
			rbr(v, mmu.read(pc + 1));
			
			m += 2;
			break;
		}
		
		case 0x18:	// CLC
		{
			c = 0;
			
			pc += 1;
			m += 2;
			break;
		}
		
		case 0xD8:	// CLD
		{
			d = 0;
			
			pc += 1;
			m += 2;
			break;
		}
		
		case 0x58:	// CLI
		{
			i = 0;
			
			pc += 1;
			m += 2;
			break;
		}
		
		case 0xB8:	// CLV
		{
			v = 0;
			
			pc += 1;
			m += 2;
			break;
		}
		
		case 0xC9:	// CMP
		{
			cmp(a, mmu.read(pc + 1));
			
			pc += 2;
			m += 2;
			break;
		}
		
		case 0xC5:
		{
			cmp(a, mmu.read(mmu.read(pc + 1)));
			
			pc += 2;
			m += 3;
			break;
		}
		
		case 0xD5:
		{
			cmp(a, zeroPageX(mmu.read(pc + 1)));
			
			pc += 2;
			m += 4;
			break;
		}
		
		case 0xCD:
		{
			cmp(a, absolute(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0xDD:
		{
			cmp(a, absoluteX(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0xD9:
		{
			cmp(a, absoluteY(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0xC1:
		{
			cmp(a, indirectX(mmu.read(pc + 1)));
			
			pc += 2;
			m += 6;
			break;
		}
		
		case 0xD1:
		{
			cmp(a, indirectY(mmu.read(pc + 1)));
			
			pc += 2;
			m += 5;
			break;
		}
		
		case 0xE0:	// CPX
		{
			cmp(x, mmu.read(pc + 1));
			
			pc += 2;
			m += 2;
			break;
		}
		
		case 0xE4:
		{
			cmp(x, mmu.read(mmu.read(pc + 1)));
			
			pc += 2;
			m += 3;
			break;
		}
		
		case 0xEC:
		{
			cmp(x, absolute(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0xC0:	// CPY
		{
			cmp(y, mmu.read(pc + 1));
			
			pc += 2;
			m += 2;
			break;
		}
		
		case 0xC4:
		{
			cmp(y, mmu.read(mmu.read(pc + 1)));
			
			pc += 2;
			m += 3;
			break;
		}
		
		case 0xCC:
		{
			cmp(y, absolute(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0xC7:	// DCP
		{
			dcp(mmu.read(pc + 1));
			
			pc += 2;
			m += 5;
			break;
		}
		
		case 0xD7:
		{
			dcp(zeroPageX(mmu.read(pc + 1)));
			
			pc += 2;
			m += 6;
			break;
		}
		
		case 0xCF:
		{
			dcp(absolute(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 6;
			break;
		}
		
		case 0xDF:
		{
			dcp(absoluteX_noClocks(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 7;
			break;
		}
		
		case 0xDB:
		{
			dcp(absoluteY_noClocks(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 7;
			break;
		}
		
		case 0xC3:
		{
			dcp(indirectX(mmu.read(pc + 1)));
			
			pc += 2;
			m += 8;
			break;
		}
		
		case 0xD3:
		{
			dcp(indirectY_noClocks(mmu.read(pc + 1)));
			
			pc += 2;
			m += 8;
			break;
		}
		
		case 0xC6:	// DEC
		{
			dec(mmu.read(pc + 1));
			
			pc += 2;
			m += 5;
			break;
		}
		
		case 0xD6:
		{
			dec(zeroPageX(mmu.read(pc + 1)));
			
			pc += 2;
			m += 6;
			break;
		}
		
		case 0xCE:
		{
			dec(absolute(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 6;
			break;
		}
		
		case 0xDE:
		{
			dec(absoluteX_noClocks(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 7;
			break;
		}
		
		case 0xCA:	// DEX
		{
			x = _dec(x);
			
			pc += 1;
			m += 2;
			break;
		}
		
		case 0x88:	// DEY
		{
			y = _dec(y);
			
			pc += 1;
			m += 2;
			break;
		}
		
		case 0x49:	// EOR
		{
			eor(mmu.read(pc + 1));
			
			pc += 2;
			m += 2;
			break;
		}
		
		case 0x45:
		{
			eor(mmu.read(mmu.read(pc + 1)));
			
			pc += 2;
			m += 3;
			break;
		}
		
		case 0x55:
		{
			eor(zeroPageX(mmu.read(pc + 1)));
			
			pc += 2;
			m += 4;
			break;
		}
		
		case 0x4D:
		{
			eor(absolute(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0x5D:
		{
			eor(absoluteX(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0x59:
		{
			eor(absoluteY(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0x41:
		{
			eor(indirectX(mmu.read(pc + 1)));
			
			pc += 2;
			m += 6;
			break;
		}
		
		case 0x51:
		{
			eor(indirectY(mmu.read(pc + 1)));
			
			pc += 2;
			m += 5;
			break;
		}
		
		case 0xE6:	// INC
		{
			inc(mmu.read(pc + 1));
			
			pc += 2;
			m += 5;
			break;
		}
		
		case 0xF6:
		{
			inc(zeroPageX(mmu.read(pc + 1)));
			
			pc += 2;
			m += 6;
			break;
		}
		
		case 0xEE:
		{
			inc(absolute(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 6;
			break;
		}
		
		case 0xFE:
		{
			inc(absoluteX_noClocks(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 7;
			break;
		}
		
		case 0xE8:	// INX
		{
			x = _inc(x);
			
			pc += 1;
			m += 2;
			break;
		}
		
		case 0xC8:	// INY
		{
			y = _inc(y);
			
			pc += 1;
			m += 2;
			break;
		}
		
		case 0xE7:	// ISC
		{
			isc(mmu.read(mmu.read(pc + 1)));
			
			pc += 2;
			m += 5;
			break;
		}
		
		case 0xF7:
		{
			isc(zeroPageX(mmu.read(pc + 1)));
			
			pc += 2;
			m += 6;
			break;
		}
		
		case 0xEF:
		{
			isc(absolute(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 6;
			break;
		}
		
		case 0xFF:
		{
			isc(absoluteX_noClocks(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 7;
			break;
		}
		
		case 0xFB:
		{
			isc(absoluteY_noClocks(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 7;
			break;
		}
		
		case 0xE3:
		{
			isc(indirectX(mmu.read(pc + 1)));
			
			pc += 2;
			m += 8;
			break;
		}
		
		case 0xF3:
		{
			isc(indirectY_noClocks(mmu.read(pc + 1)));
			
			pc += 2;
			m += 8;
			break;
		}
		
		case 0x4C:	// JMP
		{
			jmp(addr16(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			m += 3;
			break;
		}
		
		case 0x6C:
		{
			jmp(indirectAddr(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			m += 5;
			break;
		}
		
		case 0x20:	// JSR
		{
			jsr(addr16(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			m += 6;
			break;
		}
		
		case 0xA7:	//*LAX
		{
			lda(mmu.mem[mmu.read(pc + 1)]);
			ldx(a);
			
			pc += 2;
			m += 3;
			break;
		}
		
		case 0xB7:
		{
			lda(mmu.mem[zeroPageY(mmu.read(pc + 1))]);
			ldx(a);
			
			pc += 2;
			m += 4;
			break;
		}
		
		case 0xAF:
		{
			ld(a, absolute(mmu.read(pc + 1), mmu.read(pc + 2)));
			ld(x, a);
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0xBF:
		{
			ld(a, absoluteY(mmu.read(pc + 1), mmu.read(pc + 2)));
			ld(x, a);
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0xA3:
		{
			ld(a, indirectX(mmu.read(pc + 1)));
			ld(x, a);
			
			pc += 2;
			m += 6;
			break;
		}
		
		case 0xB3:
		{
			ld(a, indirectY(mmu.read(pc + 1)));
			ld(x, a);
			
			pc += 2;
			m += 5;
			break;
		}
		
		case 0xA9:	// LDA
		{
			ld(a, mmu.read(pc + 1));
			
			pc += 2;
			m += 2;
			break;
		}
		
		case 0xA5:
		{
			ld(a, mmu.read(mmu.read(pc + 1)));
			
			pc += 2;
			m += 3;
			break;
		}
		
		case 0xB5:
		{
			ld(a, zeroPageX(mmu.read(pc + 1)));
			
			pc += 2;
			m += 4;
			break;
		}
		
		case 0xAD:
		{
			ld(a, absolute(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0xBD:
		{
			ld(a, absoluteX(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0xB9:
		{
			ld(a, absoluteY(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0xA1:
		{
			ld(a, indirectX(mmu.read(pc + 1)));
			
			pc += 2;
			m += 6;
			break;
		}
		
		case 0xB1:
		{
			ld(a, indirectY(mmu.read(pc + 1)));
			
			pc += 2;
			m += 5;
			break;
		}
		
		case 0xA2:	// LDX
		{
			ld(x, mmu.read(pc + 1));
			
			pc += 2;
			m += 2;
			break;
		}
		
		case 0xA6:
		{
			ld(x, mmu.read(mmu.read(pc + 1)));
			
			pc += 2;
			m += 3;
			break;
		}
		
		case 0xB6:
		{
			ld(x, zeroPageY(mmu.read(pc + 1)));
			
			pc += 2;
			m += 4;
			break;
		}
		
		case 0xAE:
		{
			ld(x, absolute(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0xBE:
		{
			ld(x, absoluteY(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0xA0:	// LDY
		{
			ld(y, mmu.read(pc + 1));
			
			pc += 2;
			m += 2;
			break;
		}
		
		case 0xA4:
		{
			ld(y, mmu.read(mmu.read(pc + 1)));
			
			pc += 2;
			m += 3;
			break;
		}
		
		case 0xB4:
		{
			ld(y, zeroPageX(mmu.read(pc + 1)));
			
			pc += 2;
			m += 4;
			break;
		}
		
		case 0xAC:
		{
			ld(y, absolute(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0xBC:
		{
			ld(y, absoluteX(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0x4A:	// LSR
		{
			a = lsr(a);
			
			pc += 1;
			m += 2;
			break;
		}
		
		case 0x46:
		{
			u16 addr = mmu.read(pc + 1);
			mmu.write(addr, lsr(mmu.read(addr)));
			
			pc += 2;
			m += 5;
			break;
		}
		
		case 0x56:
		{
			u16 addr = zeroPageX(mmu.read(pc + 1));
			mmu.write(addr, lsr(mmu.read(addr)));
			
			pc += 2;
			m += 6;
			break;
		}
		
		case 0x4E:
		{
			u16 addr = absolute(mmu.read(pc + 1), mmu.read(pc + 2));
			mmu.write(addr, lsr(mmu.read(addr)));
			
			pc += 3;
			m += 6;
			break;
		}
		
		case 0x5E:
		{
			u16 addr = absoluteX_noClocks(mmu.read(pc + 1), mmu.read(pc + 2));
			mmu.write(addr, lsr(mmu.read(addr)));
			
			pc += 3;
			m += 7;
			break;
		}
		
		case 0xEA:	// NOP
		{
			pc += 1;
			m += 2;
			break;
		}
		
		case 0x1A:	//*NOP
		case 0x3A:
		case 0x5A:
		case 0x7A:
		case 0xDA:
		case 0xFA:
		{
			pc += 1;
			m += 2;
			break;
		}
		
		case 0x80:
		case 0x82:
		case 0x89:
		case 0xC2:
		case 0xE2:
		{
			pc += 2;
			m += 2;
			break;
		}
		
		case 0x04:
		case 0x44:
		case 0x64:
		{
			pc += 2;
			m += 3;
			break;
		}
		
		case 0x14:
		case 0x34:
		case 0x54:
		case 0x74:
		case 0xD4:
		case 0xF4:
		{
			pc += 2;
			m += 4;
			break;
		}
		
		case 0x0C:
		{
			pc += 3;
			m += 4;
			break;
		}
		
		case 0x1C:
		case 0x3C:
		case 0x5C:
		case 0x7C:
		case 0xDC:
		case 0xFC:
		{
			// make sure the extra cycle is there if need be
			absoluteX(mmu.read(pc + 1), mmu.read(pc + 2));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0x09:	// ORA
		{
			ora(mmu.read(pc + 1));
			
			pc += 2;
			m += 2;
			break;
		}
		
		case 0x05:
		{
			ora(mmu.read(mmu.read(pc + 1)));
			
			pc += 2;
			m += 3;
			break;
		}
		
		case 0x15:
		{
			ora(zeroPageX(mmu.read(pc + 1)));
			
			pc += 2;
			m += 4;
			break;
		}
		
		case 0x0D:
		{
			ora(absolute(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0x1D:
		{
			ora(absoluteX(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0x19:
		{
			ora(absoluteY(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0x01:
		{
			ora(indirectX(mmu.read(pc + 1)));
			
			pc += 2;
			m += 6;
			break;
		}
		
		case 0x11:
		{
			ora(indirectY(mmu.read(pc + 1)));
			
			pc += 2;
			m += 5;
			break;
		}
		
		case 0x48:	// PHA
		{
			push(a);
			
			pc += 1;
			m += 3;
			break;
		}
		
		case 0x08:	// PHP
		{
			push(p | 0x30);
			
			pc += 1;
			m += 3;
			break;
		}
		
		case 0x68:	// PLA
		{
			a = pop();
			
			zeroFlag(a);
			
			n = a >> 7;
			
			pc += 1;
			m += 4;
			break;
		}
		
		case 0x28:	// PLP
		{
			p &= 0x30;
			p |= pop() & 0xCF;
			
			pc += 1;
			m += 4;
			break;
		}
		
		case 0x27:	// RLA
		{
			u16 addr = mmu.read(pc + 1);
			mmu.write(addr, rla(mmu.read(addr)));
			
			pc += 2;
			m += 5;
			break;
		}
		
		case 0x37:
		{
			u16 addr = zeroPageX(mmu.read(pc + 1));
			mmu.write(addr, rla(mmu.read(addr)));
			
			pc += 2;
			m += 6;
			break;
		}
		
		case 0x2F:
		{
			u16 addr = absolute(mmu.read(pc + 1), mmu.read(pc + 2));
			mmu.write(addr, rla(mmu.read(addr)));
			
			pc += 3;
			m += 6;
			break;
		}
		
		case 0x3F:
		{
			u16 addr = absoluteX_noClocks(mmu.read(pc + 1), mmu.read(pc + 2));
			mmu.write(addr, rla(mmu.read(addr)));
			
			pc += 3;
			m += 7;
			break;
		}
		
		case 0x3B:
		{
			u16 addr = absoluteY_noClocks(mmu.read(pc + 1), mmu.read(pc + 2));
			mmu.write(addr, rla(mmu.read(addr)));
			
			pc += 3;
			m += 7;
			break;
		}
		
		case 0x23:
		{
			u16 addr = indirectX(mmu.read(pc + 1));
			mmu.write(addr, rla(mmu.read(addr)));
			
			pc += 2;
			m += 8;
			break;
		}
		
		case 0x33:
		{
			u16 addr = indirectY_noClocks(mmu.read(pc + 1));
			mmu.write(addr, rla(mmu.read(addr)));
			
			pc += 2;
			m += 8;
			break;
		}
		
		case 0x2A:	// ROL
		{
			a = rol(a);
			
			pc += 1;
			m += 2;
			break;
		}
		
		case 0x26:
		{
			u16 addr = mmu.read(pc + 1);
			mmu.write(addr, rol(mmu.read(addr)));
			
			pc += 2;
			m += 5;
			break;
		}
		
		case 0x36:
		{
			u16 addr = zeroPageX(mmu.read(pc + 1));
			mmu.write(addr, rol(mmu.read(addr)));
			
			pc += 2;
			m += 6;
			break;
		}
		
		case 0x2E:
		{
			u16 addr = absolute(mmu.read(pc + 1), mmu.read(pc + 2));
			mmu.write(addr, rol(mmu.read(addr)));
			
			pc += 3;
			m += 6;
			break;
		}
		
		case 0x3E:
		{
			u16 addr = absoluteX_noClocks(mmu.read(pc + 1), mmu.read(pc + 2));
			mmu.write(addr, rol(mmu.read(addr)));
			
			pc += 3;
			m += 7;
			break;
		}
		
		case 0x6A:	// ROR
		{
			a = ror(a);
			
			pc += 1;
			m += 2;
			break;
		}
		
		case 0x66:
		{
			u16 addr = mmu.read(pc + 1);
			mmu.write(addr, ror(mmu.read(addr)));
			
			pc += 2;
			m += 5;
			break;
		}
		
		case 0x76:
		{
			u16 addr = zeroPageX(mmu.read(pc + 1));
			mmu.write(addr, ror(mmu.read(addr)));
			
			pc += 2;
			m += 6;
			break;
		}
		
		case 0x6E:
		{
			u16 addr = absolute(mmu.read(pc + 1), mmu.read(pc + 2));
			mmu.write(addr, ror(mmu.read(addr)));
			
			pc += 3;
			m += 6;
			break;
		}
		
		case 0x7E:
		{
			u16 addr = absoluteX_noClocks(mmu.read(pc + 1), mmu.read(pc + 2));
			mmu.write(addr, ror(mmu.read(addr)));
			
			pc += 3;
			m += 7;
			break;
		}
		
		case 0x67:	// RRA
		{
			u16 addr = mmu.read(pc + 1);
			mmu.write(addr, rra(mmu.read(addr)));
			
			pc += 2;
			m += 5;
			break;
		}
		
		case 0x77:
		{
			u16 addr = zeroPageX(mmu.read(pc + 1));
			mmu.write(addr, rra(mmu.read(addr)));
			
			pc += 2;
			m += 6;
			break;
		}
		
		case 0x6F:
		{
			u16 addr = absolute(mmu.read(pc + 1), mmu.read(pc + 2));
			mmu.write(addr, rra(mmu.read(addr)));
			
			pc += 3;
			m += 6;
			break;
		}
		
		case 0x7F:
		{
			u16 addr = absoluteX_noClocks(mmu.read(pc + 1), mmu.read(pc + 2));
			mmu.write(addr, rra(mmu.read(addr)));
			
			pc += 3;
			m += 7;
			break;
		}
		
		case 0x7B:
		{
			u16 addr = absoluteY_noClocks(mmu.read(pc + 1), mmu.read(pc + 2));
			mmu.write(addr, rra(mmu.read(addr)));
			
			pc += 3;
			m += 7;
			break;
		}
		
		case 0x63:
		{
			u16 addr = indirectX(mmu.read(pc + 1));
			mmu.write(addr, rra(mmu.read(addr)));
			
			pc += 2;
			m += 8;
			break;
		}
		
		case 0x73:
		{
			u16 addr = indirectY_noClocks(mmu.read(pc + 1));
			mmu.write(addr, rra(mmu.read(addr)));
			
			pc += 2;
			m += 8;
			break;
		}
		
		case 0x40:	// RTI
		{
			p &= 0x30;
			p |= pop() & 0xCF;
			
			pc = pop();
			pc |= pop() << 8;
			
			m += 6;
			break;
		}
		
		case 0x60:	// RTS
		{
			u8 loByte = pop();
			u8 hiByte = pop();
			
			pc = addr16(loByte, hiByte);
			
			pc += 1;
			m += 6;
			break;
		}
		
		case 0x87:	// SAX
		{
			mmu.write(mmu.read(pc + 1), a & x);
			
			pc += 2;
			m += 3;
			break;
		}
		
		case 0x97:
		{
			mmu.write(zeroPageY(mmu.read(pc + 1)), a & x);
			
			pc += 2;
			m += 4;
			break;
		}
		
		case 0x8F:
		{
			mmu.write(absolute(mmu.read(pc + 1), mmu.read(pc + 2)), a & x);
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0x83:
		{
			mmu.write(indirectX(mmu.read(pc + 1)), a & x);
			
			pc += 2;
			m += 6;
			break;
		}
		
		case 0xE9:	// SBC
		{
			sbc(mmu.read(pc + 1));
			
			pc += 2;
			m += 2;
			break;
		}
		
		case 0xE5:
		{
			sbc(mmu.read(mmu.read(pc + 1)));
			
			pc += 2;
			m += 3;
			break;
		}
		
		case 0xF5:
		{
			sbc(zeroPageX(mmu.read(pc + 1)));
			
			pc += 2;
			m += 4;
			break;
		}
		
		case 0xED:
		{
			sbc(absolute(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0xFD:
		{
			sbc(absoluteX(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0xF9:
		{
			sbc(absoluteY(mmu.read(pc + 1), mmu.read(pc + 2)));
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0xE1:
		{
			sbc(indirectX(mmu.read(pc + 1)));
			
			pc += 2;
			m += 6;
			break;
		}
		
		case 0xF1:
		{
			sbc(indirectY(mmu.read(pc + 1)));
			
			pc += 2;
			m += 5;
			break;
		}
		
		case 0x38:	// SEC
		{
			c = 1;
			
			pc += 1;
			m += 2;
			break;
		}
		
		case 0xF8:	// SED
		{
			d = 1;
			
			pc += 1;
			m += 2;
			break;
		}
		
		case 0x78:	// SEI
		{
			i = 1;
			
			pc += 1;
			m += 2;
			break;
		}
		
		case 0x07:	// SLO
		{
			u16 addr = mmu.read(pc + 1);
			mmu.write(addr, slo(mmu.read(addr)));
			
			pc += 2;
			m += 5;
			break;
		}
		
		case 0x17:
		{
			u16 addr = zeroPageX(mmu.read(pc + 1));
			mmu.write(addr, slo(mmu.read(addr)));
			
			pc += 2;
			m += 6;
			break;
		}
		
		case 0x0F:
		{
			u16 addr = absolute(mmu.read(pc + 1), mmu.read(pc + 2));
			mmu.write(addr, slo(mmu.read(addr)));
			
			pc += 3;
			m += 6;
			break;
		}
		
		case 0x1F:
		{
			u16 addr = absoluteX_noClocks(mmu.read(pc + 1), mmu.read(pc + 2));
			mmu.write(addr, slo(mmu.read(addr)));
			
			pc += 3;
			m += 7;
			break;
		}
		
		case 0x1B:
		{
			u16 addr = absoluteY_noClocks(mmu.read(pc + 1), mmu.read(pc + 2));
			mmu.write(addr, slo(mmu.read(addr)));
			
			pc += 3;
			m += 7;
			break;
		}
		
		case 0x03:
		{
			u16 addr = indirectX(mmu.read(pc + 1));
			mmu.write(addr, slo(mmu.read(addr)));
			
			pc += 2;
			m += 8;
			break;
		}
		
		case 0x13:
		{
			u16 addr = indirectY_noClocks(mmu.read(pc + 1));
			mmu.write(addr, slo(mmu.read(addr)));
			
			pc += 2;
			m += 8;
			break;
		}
		
		case 0x47:	// SRE
		{
			u16 addr = mmu.read(pc + 1);
			mmu.write(addr, sre(mmu.read(addr)));
			
			pc += 2;
			m += 5;
			break;
		}
		
		case 0x57:
		{
			u16 addr = zeroPageX(mmu.read(pc + 1));
			mmu.write(addr, sre(mmu.read(addr)));
			
			pc += 2;
			m += 6;
			break;
		}
		
		case 0x4F:
		{
			u16 addr = absolute(mmu.read(pc + 1), mmu.read(pc + 2));
			mmu.write(addr, sre(mmu.read(addr)));
			
			pc += 3;
			m += 6;
			break;
		}
		
		case 0x5F:
		{
			u16 addr = absoluteX_noClocks(mmu.read(pc + 1), mmu.read(pc + 2));
			mmu.write(addr, sre(mmu.read(addr)));
			
			pc += 3;
			m += 7;
			break;
		}
		
		case 0x5B:
		{
			u16 addr = absoluteY_noClocks(mmu.read(pc + 1), mmu.read(pc + 2));
			mmu.write(addr, sre(mmu.read(addr)));
			
			pc += 3;
			m += 7;
			break;
		}
		
		case 0x43:
		{
			u16 addr = indirectX(mmu.read(pc + 1));
			mmu.write(addr, sre(mmu.read(addr)));
			
			pc += 2;
			m += 8;
			break;
		}
		
		case 0x53:
		{
			u16 addr = indirectY_noClocks(mmu.read(pc + 1));
			mmu.write(addr, sre(mmu.read(addr)));
			
			pc += 2;
			m += 8;
			break;
		}
		
		case 0x85:	// STA
		{
			mmu.write(mmu.read(pc + 1), a);
			
			pc += 2;
			m += 3;
			break;
		}
		
		case 0x95:
		{
			mmu.write(zeroPageX(mmu.read(pc + 1)), a);
			
			pc += 2;
			m += 4;
			break;
		}
		
		case 0x8D:
		{
			mmu.write(absolute(mmu.read(pc + 1), mmu.read(pc + 2)), a);
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0x9D:
		{
			mmu.write(absoluteX_noClocks(mmu.read(pc + 1), mmu.read(pc + 2)), a);
			
			pc += 3;
			m += 5;
			break;
		}
		
		case 0x99:
		{
			mmu.write(absoluteY_noClocks(mmu.read(pc + 1), mmu.read(pc + 2)), a);
			
			pc += 3;
			m += 5;
			break;
		}
		
		case 0x81:
		{
			mmu.write(indirectX(mmu.read(pc + 1)), a);
			
			pc += 2;
			m += 6;
			break;
		}
		
		case 0x91:
		{
			mmu.write(indirectY_noClocks(mmu.read(pc + 1)), a);
			
			pc += 2;
			m += 6;
			break;
		}
		
		case 0x86:	// STX
		{
			mmu.write(mmu.read(pc + 1), x);
			
			pc += 2;
			m += 3;
			break;
		}
		
		case 0x96:
		{
			mmu.write(zeroPageY(mmu.read(pc + 1)), x);
			
			pc += 2;
			m += 4;
			break;
		}
		
		case 0x8E:
		{
			mmu.write(absolute(mmu.read(pc + 1), mmu.read(pc + 2)), x);
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0x84:	// STY
		{
			mmu.write(mmu.read(pc + 1), y);
			
			pc += 2;
			m += 3;
			break;
		}
		
		case 0x94:
		{
			mmu.write(zeroPageX(mmu.read(pc + 1)), y);
			
			pc += 2;
			m += 4;
			break;
		}
		
		case 0x8C:
		{
			mmu.write(absolute(mmu.read(pc + 1), mmu.read(pc + 2)), y);
			
			pc += 3;
			m += 4;
			break;
		}
		
		case 0xAA:	// TAX
		{
			x = a;
			
			zeroFlag(x);
			n = x >> 7;
			
			pc += 1;
			m += 2;
			break;
		}
		
		case 0xA8:	// TAY
		{
			y = a;
			
			zeroFlag(y);
			n = y >> 7;
			
			pc += 1;
			m += 2;
			break;
		}
		
		case 0xBA:	// TSX
		{
			x = sp;
			
			zeroFlag(x);
			n = x >> 7;
			
			pc += 1;
			m += 2;
			break;
		}
		
		case 0x8A:	// TXA
		{
			a = x;
			
			zeroFlag(a);
			n = a >> 7;
			
			pc += 1;
			m += 2;
			break;
		}
		
		case 0x9A:	// TXS
		{
			sp = x;
			
			pc += 1;
			m += 2;
			break;
		}
		
		case 0x98:	// TYA
		{
			a = y;
			
			zeroFlag(a);
			n = a >> 7;
			
			pc += 1;
			m += 2;
			break;
		}
		
		case 0xEB:	//USBC
		{
			sbc(mmu.read(pc + 1));
			
			pc += 2;
			m += 2;
			break;
		}
		
		default:
		{
			printf("E: bad opcode $%02X at pc $%04X\n", opcode, pc);
			
			break;
		}
	}
}



inline void CPU::adc(u8 operand)
{
	bool prevC = c;
	 
	u8 prevA = a;
	
	carryFlag(a + operand + prevC);
	
	
	
	a += operand + prevC;
	
	
	
	zeroFlag(a);
	additionOverflow(prevA, operand, a);
	n = a >> 7;
}

inline void CPU::_and(u8 operand)
{
	a &= operand;
	
	zeroFlag(a);
	n = ((a >> 7) & 1);
}

inline u8 CPU::asl(u8 operand)
{
	c = operand >> 7;
	
	u8 value = operand << 1;
	
	zeroFlag(value);
	n = value >> 7;
	
	return value;
}

inline void CPU::bit(u8 operand)
{
	zeroFlag(a & operand);
	
	v = (operand >> 6) & 1;
	n = operand >> 7;
}

inline void CPU::cmp(u8 reg, u8 operand)
{
	borrowFlag(reg, operand);
	
	u8 result = reg - operand;
	
	zeroFlag(result);
	n = result >> 7;
}

inline void CPU::dcp(u16 addr)
{
	mmu.write(addr, mmu.mem[addr] - 1);
	
	cmp(a, addr);
}

inline void CPU::dec(u16 addr)
{
	mmu.write(addr, mmu.mem[addr] - 1);
	
	zeroFlag(mmu.mem[addr]);
	n = mmu.mem[addr] >> 7;
}

inline u8 CPU::_dec(u8 operand)
{
	u8 value = operand - 1;
	
	zeroFlag(value);
	n = value >> 7;
	
	return value;
}

inline void CPU::eor(u8 operand)
{
	a ^= operand;
	
	zeroFlag(a);
	n = a >> 7;
}

inline void CPU::inc(u16 addr)
{
	mmu.write(addr, mmu.mem[addr] + 1);
	
	zeroFlag(mmu.mem[addr]);
	n = mmu.mem[addr] >> 7;
}

inline u8 CPU::_inc(u8 operand)
{
	u8 value = operand + 1;
	
	zeroFlag(value);
	n = value >> 7;
	
	return value;
}

inline void CPU::isc(u16 addr)
{
	inc(addr);
	
	sbc(mmu.read(addr));
}

inline void CPU::jmp(u16 addr)
{
	pc = addr;
}

inline void CPU::jsr(u16 addr)
{
	pc += 2;
	
	push(pc >> 8);
	push(pc & 0xFF);
	
	pc = addr;
}

inline void CPU::ld(u16 addr, u8 operand)
{
	mmu.write(addr, operand);
	
	zeroFlag(mmu.mem[addr]);
	n = mmu.mem[addr] >> 7;
}

inline void CPU::lda(u8 operand)
{
	a = operand;
	
	zeroFlag(a);
	n = a >> 7;
}

inline void CPU::ldx(u8 operand)
{
	x = operand;
	
	zeroFlag(x);
	n = x >> 7;
}

inline void CPU::ldy(u8 operand)
{
	y = operand;
	
	zeroFlag(y);
	n = y >> 7;
}

inline u8 CPU::lsr(u8 operand)
{
	c = operand & 1;
	
	u8 shifted = operand >> 1;
	
	zeroFlag(operand);
	n = 0;
	
	return shifted;
}

inline void CPU::ora(u8 operand)
{
	a |= operand;
	
	zeroFlag(a);
	n = a >> 7;
}

inline void CPU::rbr(bool flag, u8 operand)
{
	pc += 2;
	
	if (flag)
	{
		u8 prevByte1 = pc >> 8;
		pc += (e8) (operand);
		
		m += 1;
		
		if (prevByte1 != (pc >> 8))
		{
			m += 1;
		}
	}
}

inline u8 CPU::rla(u8 operand)
{
	bool prevC = c;
	
	c = operand >> 7;
	
	u8 result = operand << 1;
	
	result |= prevC;
	
	_and(result);
	
	return result;
}

inline u8 CPU::rol(u8 operand)
{
	bool prevC = c;
	
	c = operand >> 7;
	
	u8 result = operand << 1;
	
	result |= prevC;
	
	zeroFlag(result);
	n = result >> 7;
	
	return result;
}

inline u8 CPU::ror(u8 operand)
{
	bool prevC = c;
	
	c = operand & 1;
	
	u8 result = operand >> 1;
	
	result |= (prevC << 7);
	
	zeroFlag(result);
	n = result >> 7;
	
	return result;
}

inline u8 CPU::rra(u8 operand)
{
	bool prevC = c;
	
	c = operand & 1;
	
	u8 result = operand >> 1;
	
	result |= (prevC << 7);
	
	adc(result);
	
	return result;
}

inline void CPU::sbc(u8 operand)
{
	bool prevC = c;
	 
	u8 prevA = a;
	
	borrowFlag(prevA, operand);
	
	
	
	a -= operand + !prevC;
	
	
	
	zeroFlag(a);
	subtractionOverflow(prevA, operand, a);
	n = a >> 7;
}

inline u8 CPU::slo(u8 operand)
{
	c = operand >> 7;
	
	u8 result = operand << 1;
	
	ora(result);
	
	return result;
}

inline u8 CPU::sre(u8 operand)
{
	c = operand & 1;
	
	u8 result = operand >> 1;
	
	eor(result);
	
	return result;
}



inline void CPU::carryFlag(int result)
{
	if (result > 0xFF)
	{
		c = 1;
	}
	
	else
	{
		c = 0;
	}
}

inline void CPU::borrowFlag(int minuend, int subtrahend)
{
	if (minuend >= subtrahend)
	{
		c = 1;
	}
	
	else
	{
		c = 0;
	}
}

inline void CPU::zeroFlag(u8 operand)
{
	z = (operand == 0) ? 1 : 0;
}

inline void CPU::additionOverflow(int operand1, int operand2, u8 result)
{
	int prevSignBit1 = (operand1 >> 7) & 1;
	int prevSignBit2 = (operand2 >> 7) & 1;
	int currSignBit = (result >> 7) & 1;
	
	if ((prevSignBit1 == prevSignBit2) && (currSignBit != prevSignBit1))
	{
		v = 1;
	}
	
	else
	{
		v = 0;
	}
}

inline void CPU::subtractionOverflow(int operand1, int operand2, u8 result)
{
	int prevSignBit1 = (operand1 >> 7) & 1;
	int prevSignBit2 = (operand2 >> 7) & 1;
	int currSignBit = (result >> 7) & 1;
	
	if ((prevSignBit1 != prevSignBit2) && (currSignBit != prevSignBit1))
	{
		v = 1;
	}
	
	else
	{
		v = 0;
	}
}



inline u16 CPU::zeroPageX(u8 addr)
{
	return (addr + x) & 0xFF;
}

inline u16 CPU::zeroPageY(u8 addr)
{
	return (addr + y) & 0xFF;
}

inline u16 CPU::absolute(u8 loByte, u8 hiByte)
{
	return addr16(loByte, hiByte);
}

inline u16 CPU::absoluteX(u8 loByte, u8 hiByte)
{
	u16 addr = addr16(loByte, hiByte);
	
	if (((addr & 0xFF) + x > 0xFF))
	{
		m += 1;
	}
	
	return addr + x;
}

inline u16 CPU::absoluteX_noClocks(u8 loByte, u8 hiByte)
{
	u16 addr = addr16(loByte, hiByte);
	
	return addr + x;
}

inline u16 CPU::absoluteY(u8 loByte, u8 hiByte)
{
	u16 addr = addr16(loByte, hiByte);
	
	if ((addr & 0xFF) + y > 0xFF)
	{
		m += 1;
	}
	
	return addr + y;
}

inline u16 CPU::absoluteY_noClocks(u8 loByte, u8 hiByte)
{
	u16 addr = addr16(loByte, hiByte);
	
	return addr + y;
}



inline u16 CPU::indirectAddr(u8 loByte, u8 hiByte)
{
	u16 loByteAddr = addr16(loByte, hiByte);
	u16 hiByteAddr = addr16((loByte + 1) & 0xFF, hiByte);
	
	return addr16(mmu.read(loByteAddr), mmu.read(hiByteAddr));
}



inline u16 CPU::indirectX(u8 addr)
{
	return indirectAddr((addr + x) & 0xFF, 0x00);
}

inline u16 CPU::indirectY(u8 addr)
{
	u16 indAddr = addr16(mmu.read(addr), mmu.read((addr + 1) & 0xFF));
	
	if ((indAddr & 0xFF) + y > 0xFF)
	{
		m += 1;
	}
	
	return indAddr + y;
}

inline u16 CPU::indirectY_noClocks(u8 addr)
{
	return addr16(mmu.read(addr), mmu.read(addr + 1)) + y;
}



inline u16 CPU::addr16(u8 loByte, u8 hiByte)
{
	return (hiByte << 8) | loByte;
}

inline void CPU::push(u8 value)
{
	mmu.write(0x100 + sp--, value);
}

inline u8 CPU::pop()
{
	return mmu.read(0x100 + ++sp);
}