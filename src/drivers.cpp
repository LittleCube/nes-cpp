#include <SDL2/SDL.h>

#include <drivers.hpp>
#include <common.hpp>

SDL_Window *window;
SDL_Renderer *renderer;

SDL_Event e;

void initDrivers()
{
	quit = false;
	
	SDL_Init(SDL_INIT_VIDEO);
	
	initVideo();
}

void initVideo()
{
	window = SDL_CreateWindow("GrumpNES", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, W * SCALE, H * SCALE, 0);
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	
	SDL_RenderSetLogicalSize(renderer, W, H);
	
	if (window == NULL)
	{
		printf("Could not create window: %s\n", SDL_GetError());
		exit(1);
	}
}

void setPixel(int x, int y, Color color)
{
	SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, 0xFF);
	
	SDL_RenderDrawPoint(renderer, x, y);
}

void setPixels(Color gfx[])
{
	for (int y = 0; y < H; y++)
	{
		for (int x = 0; x < W; x++)
		{
			setPixel(x, y, gfx[x + (y * W)]);
		}
	}
	
	update();
}

void update()
{
	SDL_RenderPresent(renderer);
	
	while (SDL_PollEvent(&e))
	{
		switch (e.type)
		{
			case SDL_WINDOWEVENT:
			{
				if (e.window.event == SDL_WINDOWEVENT_CLOSE)
				{
					quit = true;
				}
				
				break;
			}
			
			case SDL_QUIT:
			{
				quit = true;
				
				break;
			}
		}
	}
	
	if (quit)
	{
		exitGfx();
		
		quitNES();
	}
}

void exitGfx()
{
	SDL_DestroyWindow(window);
	SDL_Quit();
}