#include <ppu.hpp>
#include <drivers.hpp>

#include <main.hpp>

PPU::PPU()
{
	COLORS = new Color[4];
	
	COLORS[0].r = 0x00;
	COLORS[0].g = 0x00;
	COLORS[0].b = 0x00;
	
	COLORS[1].r = 0x55;
	COLORS[1].g = 0x55;
	COLORS[1].b = 0x55;
	
	COLORS[2].r = 0xA5;
	COLORS[2].g = 0xA5;
	COLORS[2].b = 0xA5;
	
	COLORS[3].r = 0xFF;
	COLORS[3].g = 0xFF;
	COLORS[3].b = 0xFF;
	
	gfx = new Color[W * H];
	
	mem = new u8[0x4000];
	
	ppuctrl = 0;
	ppumask = 0;
	ppustatus = 0;
	oamaddr = 0;
	oamdata = 0;
	ppuscroll = 0;
	ppudata = 0;
	
	inPPUScroll = 0;
	inPPUAddr = 0;
	
	latch = 0;
	
	latchPos = true;
	oddFrame = false;
	
	lineClocks = 0;
	
	lx = 0;
	ly = 0;
}

void PPU::clock(int clocks)
{
	clocks *= 3;
	
	while (clocks-- > 0)
	{
		if (lineClocks >= 1 && lineClocks <= 256)
		{
			setGfx(lx, ly, COLORS[getTilePixel(lx % 8, ly % 8, mem[(0x2000 + (0x400 * (ppuctrl & 3))) + (lx / 8) + ((ly / 8) * 32)])]);
			
			lx++;
		}
		
		else if (lineClocks == 257)
		{
			lineClocks = -1;
			
			lx = 0;
			
			ly++;
			ly %= 240;
			
			setPixels(gfx);
		}
		
		lineClocks++;
	}
}

inline void PPU::setGfx(int x, int y, Color c)
{
	gfx[x + (y * W)] = c;
}

inline int PPU::getTilePixel(int x, int y, int tileNo)
{
	bool bit0 = (mem[(tileNo * 16) + y] >> abs(x - 7)) & 1;
	bool bit1 = (mem[(tileNo * 16) + y + 8] >> abs(x - 7)) & 1;
	
	return (bit1 << 1) | bit0;
}