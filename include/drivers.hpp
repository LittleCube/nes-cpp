#pragma once

#include <ppu.hpp>

const int W = 256;
const int H = 240;

const int SCALE = 2;

void initDrivers();

void initVideo();

void setPixel(int x, int y, Color color);
void setPixels(Color* gfx);

void update();

void exitGfx();