#pragma once

#include <common.hpp>

#define PPUCTRL 0x2000
#define PPUMASK 0x2001
#define PPUSTATUS 0x2002
#define OAMADDR 0x2003
#define OAMDATA 0x2004
#define PPUSCROLL 0x2005
#define PPUADDR 0x2006
#define PPUDATA 0x2007

#define ppuctrl cpu.mmu.mem[0x2000]
#define ppumask cpu.mmu.mem[0x2001]
#define ppustatus cpu.mmu.mem[0x2002]
#define oamaddr cpu.mmu.mem[0x2003]
#define oamdata cpu.mmu.mem[0x2004]
#define ppuscroll cpu.mmu.mem[0x2005]
#define ppuaddr cpu.mmu.mem[0x2006]
#define ppudata cpu.mmu.mem[0x2007]

struct Color
{
	u8 r;
	u8 b;
	u8 g;
};

class PPU
{
	Color* COLORS;
	
public:
	Color* gfx;
	
	u8* mem;
	
	union
	{
		struct
		{
			u8 lowPPUScroll;
			u8 highPPUScroll;
		};
		
		u16 inPPUScroll;
	};
	
	union
	{
		struct
		{
			u8 lowPPUAddr;
			u8 highPPUAddr;
		};
		
		u16 inPPUAddr;
	};
	
	u8 latch;
	
	bool latchPos;
	bool oddFrame;
	
	int lineClocks;
	
	int lx;
	int ly;
	
	PPU();
	
	void clock(int clocks);
	
	inline void setGfx(int x, int y, Color c);
	inline int getTilePixel(int x, int y, int tileNo);
};