#pragma once

#include <common.hpp>

#include <memory.hpp>

class CPU
{
public:
	static const int C = 0;
	static const int Z = 1;
	static const int I = 2;
	static const int D = 3;
	static const int B = 4;
	static const int V = 6;
	static const int N = 7;
	
	union
	{
		struct
		{
			bool c : 1;
			bool z : 1;
			bool i : 1;
			bool d : 1;
			bool b : 1;
			bool   : 1;
			bool v : 1;
			bool n : 1;
		};
		
		u8 p;
	};
	
	int m;
	long totalCycles;
	
	u8 a;
	u8 x;
	u8 y;
	
	u8 sp;
	
	u16 pc;
	
	Memory mmu;
	
	CPU();
	void init();
	void reset();
	
	void loadROM(char* path);
	
	void cycle();
	
	void adc(u8 operand);
	void _and(u8 operand);
	u8 asl(u8 operand);
	void bit(u8 operand);
	void cmp(u8 reg, u8 operand);
	void dcp(u16 addr);
	void dec(u16 addr);
	u8 _dec(u8 operand);
	void eor(u8 operand);
	void inc(u16 addr);
	u8 _inc(u8 operand);
	void isc(u16 addr);
	void jmp(u16 addr);
	void jsr(u16 addr);
	void ld(u16 addr, u8 operand);
	void lda(u8 operand);
	void ldx(u8 operand);
	void ldy(u8 operand);
	u8 lsr(u8 operand);
	void ora(u8 operand);
	void rbr(bool flag, u8 operand);
	u8 rla(u8 operand);
	u8 rol(u8 operand);
	u8 ror(u8 operand);
	u8 rra(u8 operand);
	void sbc(u8 operand);
	u8 slo(u8 operand);
	u8 sre(u8 operand);
	
	void carryFlag(int result);
	void borrowFlag(int minuend, int subtrahend);
	void zeroFlag(u8 operand);
	void additionOverflow(int operand1, int operand2, u8 result);
	void subtractionOverflow(int operand1, int operand2, u8 result);
	
	u16 zeroPage(u8 addr);
	u16 zeroPageX(u8 addr);
	u16 zeroPageY(u8 addr);
	
	u16 absolute(u8 loByte, u8 hiByte);
	u16 absoluteX(u8 loByte, u8 hiByte);
	u16 absoluteX_noClocks(u8 loByte, u8 hiByte);
	u16 absoluteY(u8 loByte, u8 hiByte);
	u16 absoluteY_noClocks(u8 loByte, u8 hiByte);
	
	u16 indirectAddr(u8 loByte, u8 hiByte);
	
	u16 indirectX(u8 addr);
	u16 indirectY(u8 addr);
	u16 indirectY_noClocks(u8 addr);
	
	u16 addr16(u8 loByte, u8 hiByte);
	void push(u8 value);
	u8 pop();
};