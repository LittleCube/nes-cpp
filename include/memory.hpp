#pragma once

#include <mapper.hpp>

class Memory
{
public:
	Mapper mapper;
	
	u8* mem;
	u8* ppumem;
	
	Memory();
	void init();
	
	void loadROM(char* path);
	
	u8 read(u16 addr);
	void write(u16 addr, u8 value);
};