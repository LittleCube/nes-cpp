#pragma once

#include <stdio.h>
#include <stdlib.h>

#include <iostream>

using namespace std;

typedef uint8_t u8;
typedef uint16_t u16;

typedef int8_t e8;
typedef int16_t e16;

extern bool quit;

extern void quitNES();